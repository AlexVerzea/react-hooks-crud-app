import React from "react";
import ReactDOM from "react-dom";
import DemoApp from "../context-reducer-demo/DemoApp";
import * as serviceWorker from "./serviceWorker";

/**
 * The Context API provides a way to pass data through the component tree
 * without having to pass props down manually at every level. Need access to
 * Context throughout the app component, so need to export it.
 */
export const UserContext = React.createContext();

const username = "Dave";

/**
 * React.createContext() returns an object with 2 values, Provider for the provider
 * of the props and Consumer for the consumer of the props.
 */
ReactDOM.render(
  <UserContext.Provider value={username}>
    <DemoApp />
  </UserContext.Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
