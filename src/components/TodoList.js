import React, { useContext } from "react";
import TodosContext from "../context";
import pen from "../assets/pen.png";
import bin from "../assets/bin.png";
import axios from "axios";

export default function TodoList() {
  const { state, dispatch } = useContext(TodosContext);
  const title =
    state.todos.length > 0 ? `${state.todos.length} Todos` : "Nothing to do!";

  return (
    <div className="container mx-auto max-w-md text-center font-mono">
      <h1 className="text-bold">{title}</h1>
      <ul className="list-reset text-white p-0">
        {state.todos.map(todo => (
          <li
            key={todo.id}
            className="flex items-center bg-orange-dark border-black border-dashed border-2 my-2 py-4"
          >
            <span
              onClick={async () => {
                const response = await axios.patch(
                  `https://todos-api-nypzfosays.now.sh/todos/${todo.id}`,
                  {
                    complete: !todo.complete
                  }
                );
                dispatch({ type: "TOGGLE_TODO", payload: response.data });
              }}
              className={`flex-1 ml-12 cursor-pointer ${todo.complete &&
                "line-through text-grey-darkest"}`}
            >
              {todo.text}
            </span>
            <button
              className="btn btn-blue"
              onClick={() =>
                dispatch({ type: "SET_CURRENT_TODO", payload: todo })
              }
            >
              <img src={pen} alt="Edit Icon" className="h-6" />
            </button>
            <button
              onClick={async () => {
                await axios.delete(
                  `https://todos-api-nypzfosays.now.sh/todos/${todo.id}`
                );
                dispatch({ type: "REMOVE_TODO", payload: todo });
              }}
              className="btn btn-blue ml-2 mr-2"
            >
              <img src={bin} alt="Delete Icon" className="h-6" />
            </button>
          </li>
        ))}
      </ul>
    </div>
  );
}
