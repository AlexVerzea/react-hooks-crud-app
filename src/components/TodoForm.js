import React, { useState, useEffect, useContext } from "react";
import TodosContext from "../context";
import uuidv4 from "uuid/v4";
import axios from "axios";

export default function TodoForm() {
  const [todo, setTodo] = useState("");
  /** Get only currentTodo from state and set a default value of empty in case
   * we try to access currentTodo but the value is not yet set.
   */

  const {
    state: { currentTodo = {} },
    dispatch
  } = useContext(TodosContext);

  const handleSubmit = async event => {
    event.preventDefault();
    if (currentTodo.text) {
      const response = await axios.patch(
        `https://todos-api-nypzfosays.now.sh/todos/${currentTodo.id}`,
        {
          text: todo
        }
      );
      dispatch({ type: "UPDATE_TODO", payload: response.data });
    } else {
      const response = await axios.post(
        "https://todos-api-nypzfosays.now.sh/todos",
        {
          id: uuidv4(),
          text: todo,
          complete: false
        }
      );
      dispatch({ type: "ADD_TODO", payload: response.data });
    }
    setTodo("");
  };

  /**
   * Run only when the value of currentTodo changes.
   */
  useEffect(() => {
    if (currentTodo.text) {
      setTodo(currentTodo.text);
    } else {
      setTodo("");
    }
  }, [currentTodo]);

  return (
    <form onSubmit={handleSubmit} className="flex justify-center p-5">
      <input
        type="text"
        className="border-black border-solid border-2"
        onChange={event => setTodo(event.target.value)}
        value={todo}
      />
    </form>
  );
}
