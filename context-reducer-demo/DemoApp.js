import React, { useContext, useReducer } from "react";
import { UserContext } from "../src/index";

const initialState = {
  count: 0
};

function reducer(state, action) {
  switch (action.type) {
    case "increment":
      return {
        count: state.count + 1
      };
    case "decrement":
      return {
        count: state.count - 1
      };
    case "reset":
      return initialState;
    default:
      return initialState;
  }
}

export default function DemoApp() {
  /**
   * This is the value passed in by UserContext.
   */
  const value = useContext(UserContext);

  /**
   * useReducer allows us to replace the use of Redux for global state management.
   * Inputs: a reducer function and the initial state.
   * Outputs: updated state and a dispatch function (to dispatch actions).
   */
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <div>
      {/* The 'type' property of the action here must match the value of the switch-case above */}
      Count: {state.count}
      <button
        className="border m-1 p-1"
        onClick={() => dispatch({ type: "increment" })}
      >
        Increment
      </button>
      <button
        className="border m-1 p-1"
        onClick={() => dispatch({ type: "decrement" })}
      >
        Decrement
      </button>
      <button
        className="border m-1 p-1"
        onClick={() => dispatch({ type: "reset" })}
      >
        Reset
      </button>
    </div>
  );
}
